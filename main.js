import { sendItem } from './post.js';
import { getItems, displayItems } from './items.js';

// Get and display items
displayItems();

// Handle new item submission
document.getElementById('submit').addEventListener('click', () => {
  const item = getFormValues();
  sendItem(item);
});
