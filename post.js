// post.js
export { sendItem }
// DOM elements 
const form = document.querySelector('form');
const typeSelect = document.getElementById('type');
// etc

// Handle form submission
form.addEventListener('submit', handleSubmit);

async function handleSubmit(e) {

  // Prevent default submit
  e.preventDefault();

  // Get form values
  // Construct item object

  try {
    await sendItem(item);
    console.log('Item sent!');
  } catch (err) {
    console.error('Error sending item:', err);
  }

}

async function sendItem(item) {

  try {
    const response = await fetch('/api/items', {
      method: 'POST', 
      body: JSON.stringify(item)
    });
    
    if(!response.ok) {
      throw new Error('Network response was not ok');
    }

  } catch (error) {
    console.error('Error sending item:', error);
    throw error;
  }

}

// Get saved items from API
async function getItems() {

  try {
    const response = await fetch('/api/items');
    const data = await response.json();
    return data;

  } catch (err) {
    console.error('Error getting items:', err);
    return [];
  }

}

    // Display items on page
    async function displayItems() {

        const items = await getItems();
      
        items.forEach(item => {
          if(item.type === 'announcement') {
            displayAnnouncement(item);
          }
          else if(item.type === 'assignment') {
            displayAssignment(item); 
          }
          else if(item.type === 'material') {
            displayMaterial(item);
          }
        });
      
      }      
  
  function displayAnnouncement(item) {
    const li = document.createElement('li');
    li.textContent = item.title;
  
    document.querySelector('.announcements').appendChild(li);
  }
  
  function displayAssignment(item) {
    const li = document.createElement('li');
    li.innerHTML = `
      <h3>${item.title} (${item.type})</h3>
      <p>${item.instructions}</p>
      <p>${item.content}</p>
      <p>Links: ${item.links}</p>
    `;
  
    document.querySelector('.assignments').appendChild(li);  
  }
  
  function displayMaterial(item) {

    const li = document.createElement('li');
  
    li.innerHTML = `
      <h3>${item.title} (${item.type})</h3>
      <p>${item.instructions}</p>
      <p>${item.content}</p>
      <p>Links: ${item.links}</p>
    `;
  
    document.querySelector('.materials').appendChild(li);
  
  }

displayItems();
