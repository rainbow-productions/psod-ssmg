// jQuery for toggling assignment clicks
$(document).ready(function() {
    $('.assignments li').click(function() {
      $(this).toggleClass('active');
    });
    
    // Additional code to post items
    
    $('#post-assignment').click(function() {
      $('.assignments').append('<li>New assignment</li>'); 
    });
  
    $('#post-material').click(function() {
      $('.materials').append('<li>New material</li>');
    });
  
    $('#post-announcement').click(function() {
      $('.announcements').append('<li>New announcement</li>');
    });
  });
  